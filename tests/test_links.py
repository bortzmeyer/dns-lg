import re

import requests

from data import servers, url, testdom

# Yes, real parsing of the HTML would be better but we are lazy so we
# use regexps (not a big problem in practice, since it is an HTML that
# *we* produce).

def test_html_soa():
    for server in servers:
        response = requests.get("%s%s/%s/SOA?format=HTML" % (url % servers[server]["port"], servers[server]["base"], testdom))
        assert response.status_code == 200
        code = response.text
        match = re.search("master server <a href=\"(.+?)\" class=\"hostname\">", code)
        assert match
        response = requests.get("%s%s" % (url % servers[server]["port"], match.group(1)))
        assert response.status_code == 200
        
def test_html_ns():
    for server in servers:
        response = requests.get("%s%s/%s/NS?format=HTML" % (url % servers[server]["port"], servers[server]["base"], testdom))
        assert response.status_code == 200
        code = response.text
        match = re.search("Name Servers: <a href=\"(.+?)\" class=\"hostname\">", code)
        assert match
        response = requests.get("%s%s" % (url % servers[server]["port"], match.group(1)))
        assert response.status_code == 200
        
def test_html_addr():
    for server in servers:
        response = requests.get("%s%s/%s/ADDR?format=HTML" % (url % servers[server]["port"], servers[server]["base"], testdom))
        assert response.status_code == 200
        code = response.text
        match = re.search("IP address: <a href=\"(.+?)\" class=\"address\">", code)
        assert match
        path = re.sub("&amp;", "&", match.group(1))
        response = requests.get("%s%s" % (url % servers[server]["port"], path))
        assert response.status_code == 200 
        
# def test_gemini_soa(): Currently (2023-01-25), no links in SOA output

def test_gemini_ns():
    for server in servers:
        response = requests.get("%s%s/%s/NS?format=GEMINI" % (url % servers[server]["port"], servers[server]["base"], testdom))
        assert response.status_code == 200
        code = response.text
        match = re.search("^=> (.+?) Name server:", code, re.MULTILINE)
        assert match
        response = requests.get("%s%s" % (url % servers[server]["port"], match.group(1)))
        assert response.status_code == 200
        
def test_gemini_addr():
    for server in servers:
        response = requests.get("%s%s/%s/ADDR?format=GEMINI" % (url % servers[server]["port"], servers[server]["base"], testdom))
        assert response.status_code == 200
        code = response.text
        match = re.search("^=> (.+?) IP address:", code, re.MULTILINE)
        assert match
        response = requests.get("%s%s" % (url % servers[server]["port"], match.group(1)))
        assert response.status_code == 200
        

