import requests

from data import servers, url

def test_ordinary():
    response = requests.get("%s/9.9.9.9?reverse=1" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200

def test_response():
    response = requests.get("%s/9.9.9.9?reverse=1" % (url % servers["nobase"]["port"]))
    assert "dns9.quad9.net" in response.text 
    
def test_nxdomain():
    response = requests.get("%s/192.0.2.1?reverse=1" % (url % servers["nobase"]["port"]))
    assert response.status_code == 404



