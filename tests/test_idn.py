import requests

from data import servers, url

def test_normal():
    response = requests.get("%s/www.potamochère.fr" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200
    
def test_emoji():
    response = requests.get("%s/www.🐉.fr" % (url % servers["nobase"]["port"]))
    assert response.status_code == 404 # Does not exist but may be we should intercept it before?
