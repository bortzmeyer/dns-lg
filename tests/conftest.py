import wsgiref.simple_server
import multiprocessing

# https://docs.pytest.org/
import pytest

# Our code
import DNSLG

from data import servers

email_admin = "foobar@invalid"
url_doc = None
url_css = None

@pytest.fixture(autouse="yes", scope="session")
def server():
    instances = []
    for server in servers:
        querier = DNSLG.Querier(email_admin, url_doc, url_css, base_url=servers[server]["base"])
        httpd = wsgiref.simple_server.make_server("", servers[server]["port"], querier.application)
        p = multiprocessing.Process(target=httpd.serve_forever)
        p.start()
        instances.append(p)
    yield None
    for instance in instances:
        instance.terminate()
    
