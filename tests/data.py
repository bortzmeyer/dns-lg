servers = {
    "nobase": {
        "port": 8080,
        "base": ""},
    "rootbase": {
        "port": 8081,
        "base": "/"},
    "dnsbase": {
        "port": 8082,
        "base": "/dns"}}

url = "http://localhost:%s"
# testdom = "aemarielle.com" # No PTR, so breaks ADDR link tests
testdom = "paris-web.fr"


