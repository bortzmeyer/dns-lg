import requests

from data import servers, url, testdom

def test_nobase():
    for server in [servers["nobase"], servers["rootbase"]]:
        response = requests.get("%s/%s" % (url % server["port"], testdom))
        assert response.status_code == 200
    
def test_base():
    response = requests.get("%s%s/%s" % \
                            (url % servers["dnsbase"]["port"], servers["dnsbase"]["base"], testdom))
    assert response.status_code == 200
    
