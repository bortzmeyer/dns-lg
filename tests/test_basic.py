import requests

from data import servers, url

def test_nobase():
    for server in [servers["nobase"], servers["rootbase"]]:
        response = requests.get("%s/www.bortzmeyer.org" % (url % server["port"]))
        assert response.status_code == 200
    
def test_base():
    response = requests.get("%s%s/www.bortzmeyer.org" % (url % servers["dnsbase"]["port"], servers["dnsbase"]["base"]))
    assert response.status_code == 200
    
def test_nxdomain():
    for server in servers:
        response = requests.get("%s/doesnotexist" % (url % servers[server]["port"]))
        assert response.status_code == 404

def test_wrongbase():
    response = requests.get("%s%s/www.bortzmeyer.org" % (url % servers["dnsbase"]["port"], "/doesnotexist"))
    assert response.status_code == 404

def test_doubleslash():
    for server in [servers["nobase"], servers["rootbase"]]:
        response = requests.get("%s//www.bortzmeyer.org" % (url % server["port"]))
        assert response.status_code == 200 


