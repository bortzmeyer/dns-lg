#!/usr/bin/env python3

# Test by executing 'pytest'
# https://docs.pytest.org/

# https://requests.readthedocs.io/en/master/
import requests

import re

formats = {'': 'text/html; charset=UTF-8', # Default
           'HTML': 'text/html; charset=UTF-8',
           'TXT': 'text/plain; charset=UTF-8',
           'ZONE': 'text/dns',
           'JSON': 'application/json',
           'XML': 'application/xml'}

# TODO for the XML and (X)HTML outputs, test the validity (or at least
# the well-formedness) of the XML

def fstring(f, add=False):
    if f is None or f == '':
        return ''
    else:
        if add:
            return '&format=%s' % f
        else:
            return '?format=%s' % f

def sstring(f):
    try:
        (left, right) = formats[f].split(';')
        return left
    except ValueError:
        return formats[f]

def test_basic(prefix):
    r = requests.get(prefix)
    assert  r.status_code == 200 and r.headers['content-type'] == 'text/html; charset=UTF-8' and \
        'No data was found' in r.text 
    
def test_notexist(prefix):
    r = requests.get(prefix + 'doesnotexistatall')
    assert  r.status_code == 404 and 'does not exist' in r.text 

def test_unknown_format(prefix):
    r = requests.get(prefix + 'icann.org/?format=XXXX')
    assert  r.status_code == 400 and 'Unsupported format "XXXX"' in r.text 

def test_defaulttype(prefix): # ADDR by default
    for format in formats:
        r = requests.get(prefix + 'www.ietf.org/%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            '104.20.' in r.text, 'format is %s' % format

def test_addr(prefix):
    for format in formats:
        r = requests.get(prefix + 'www.ietf.org/ADDR%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            '104.20.' in r.text, 'format is %s' % format

def test_addr_v4(prefix):
    for format in formats:
        r = requests.get(prefix + 'www.ietf.org/A%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            '104.20.' in r.text, 'format is %s' % format

def test_addr_v6(prefix):
    for format in formats:
        r = requests.get(prefix + 'www.ietf.org/AAAA%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            '2606:4700:10::' in r.text, 'format is %s' % format

def test_mx(prefix):
    for format in formats:
        r = requests.get(prefix + 'framasoft.org/MX%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'mail.framasoft.org' in r.text, 'format is %s' % format

def test_naptr(prefix):
     for format in formats:
         r = requests.get(prefix + 'canalplusoverseas.fr/NAPTR%s' % fstring(format))
         assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
             '_sip._tcp.canalplusoverseas.fr' in r.text, 'format is %s' % format

def test_txt(prefix):
    for format in formats:
        r = requests.get(prefix + 'fr/TXT%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'RRs processed' in r.text, 'format is %s' % format

def test_dnskey(prefix):
    for format in formats:
        r = requests.get(prefix + 'paypal.com/DNSKEY%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
             ((format == 'JSON' and '"Algorithm": 5' in r.text) or
             (format == 'XML' and 'algorithm="5"' in r.text) or
             ('RSASHA1' in r.text)), 'format is %s' % format  

def test_loc(prefix):
    for format in formats:
        r = requests.get(prefix + '52100.cp.bortzmeyer.fr/LOC%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            '48' in r.text, 'format is %s' % format

def test_ns(prefix):
    for format in formats:
        r = requests.get(prefix + 'frmug.org/NS%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'ns.eu.org' in r.text, 'format is %s' % format

def test_srv(prefix):
    for format in formats:
        r = requests.get(prefix + '_sipfederationtls._tcp.bretagne-ouest.cci.bzh/SRV%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'sipfed.online.lync.com' in r.text, 'format is %s' % format

def test_ds(prefix):
    for format in formats:
        r = requests.get(prefix + 'cyberstructure.fr/DS%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
             ((format == 'ZONE' and 'DS' in r.text) or
             (format == 'TXT' and 'Delegation of signature:' in r.text) or
             (format == 'JSON' and '"Type": "DS"' in r.text) or
             (format == 'XML' and '<RRSet type="DS"' in r.text ) or
             ('Secure Delegation:' in r.text)), 'format is %s' % format

def test_ds_algos(prefix):
    for format in formats:
        r = requests.get(prefix + 'tf/DS%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            (((format == 'ZONE' or format == 'HTML' or format == '' or format == 'TXT') and 'ECDSAP256' in r.text) or
             (format == 'JSON' and '"Type": "DS"' in r.text) or
             (format == 'XML' and '<RRSet type="DS"' in r.text)), 'format is %s' % format

def test_nsec3param(prefix):
    for format in formats:
        r = requests.get(prefix + 'pm/NSEC3PARAM%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'].startswith(formats[format]) and \
             ((format == 'ZONE' and 'NSEC3PARAM' in r.text) or
             (format == 'TXT' and 'NSEC3PARAM:' in r.text) or
             (format == 'JSON' and '"Iterations": 1' in r.text) or
             (format == 'XML') or # Nothing in the XML output?
             ('NSEC3 parameters:' in r.text)), 'format is %s' % format

def test_uri(prefix):
    for format in formats:
        r = requests.get(prefix + '78000.cp.bortzmeyer.fr/URI%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'https://www.openstreetmap.org/' in r.text, 'format is %s' % format

def test_cname(prefix): 
    for format in formats:
        r = requests.get(prefix + 'www.elysee.fr/CNAME%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'incapdns.net' in r.text, 'format is %s' % format

# Once #40 is done, we'll have to use another type
def test_unformatted_type(prefix):
    for format in formats:
        r = requests.get(prefix + '_443._tcp.doh.bortzmeyer.fr/TLSA%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
             ((format == 'ZONE' and 'TYPE52' in r.text) or
             (format == 'JSON' and '"Type": "unknown 52"' in r.text) or
             (format == 'XML' and '<binaryRR rtype="52"' in r.text) or
             ('Unknown record type' in r.text)), 'format is %s' % format

def test_rev_v4(prefix):
    for format in formats:
        r = requests.get(prefix + '194.0.9.1?reverse=1%s' % fstring(format, add=True))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'nic.fr' in r.text, 'format is %s' % format

def test_rev_v6(prefix):
    for format in formats:
        r = requests.get(prefix + '2001:678:c::1?reverse=1%s' % fstring(format, add=True))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'nic.fr' in r.text, 'format is %s' % format

def test_dnssec(prefix):
    for format in formats:
        r = requests.get(prefix + 'cyberstructure.fr?dodnssec=1%s' % fstring(format, add=True))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            (((format == 'HTML' or format == '' or format == 'TXT') and 'Authentic Data' in r.text) or
             (format == 'ZONE' and 'Flags: ad' in r.text) or
             (format == 'JSON' and '"AD": true' in r.text) or
             (format == 'XML' and '<ad>1</ad>' in r.text) or
             ('Authentic data' in r.text)), 'format is %s' % format

# TODO test with cd=1

def test_tcp(prefix):
    for format in formats:
        r = requests.get(prefix + 'fr.wikipedia.org/A?tcp=1%s' % fstring(format, add=True))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format]

def test_bufsize(prefix):
    for format in formats:
        r = requests.get(prefix + 'fr.wikipedia.org/A?buffersize=512%s' % fstring(format, add=True))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format]

def test_both(prefix):
    for format in formats:
        r = requests.get(prefix + 'fr.wikipedia.org/A?tcp=1&buffersize=1024%s' % fstring(format, add=True))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format]

def test_resolver(prefix):
    for resolver in ['1.1.1.1', '9.9.9.9']:
        for format in formats:
            r = requests.get(prefix + 'fr.wikipedia.org/A?server=%s%s' % (resolver, fstring(format, add=True)))
            assert  r.status_code == 200 and r.headers['content-type'] == formats[format]

# TODO DNAME types

def test_root(prefix):
    for format in formats:
        r = requests.get(prefix + './NS%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'f.root-servers.net' in r.text, 'format is %s' % format
        r = requests.get(prefix + 'root/NS%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'f.root-servers.net' in r.text, 'format is %s' % format
        r = requests.get(prefix + '%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format], 'format is %s' % format # TODO content

# TODO other classes

def test_dnssec(prefix):
     for format in formats:
        r = requests.get(prefix + 'servfail.nl%s' % fstring(format, add=True))
        assert (r.status_code == 504 or r.status_code == 404) and r.headers['content-type'].startswith('text/plain')

# TODO special characters

# TODO other methods than GET

def test_no_such_qtype(prefix):
    for format in formats:
        r = requests.get(prefix + 'gouvernement.fr/XXXX%s' % fstring(format))
        assert  r.status_code == 400 and r.headers['content-type'].startswith('text/plain') and \
            'Record type XXXX does not exist\n' == r.text, 'format is %s' % format

def test_nodata(prefix):
    for format in formats:
        r = requests.get(prefix + 'com/AAAA%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            ((format == 'TXT') or
             (format == 'ZONE') or
             (format == 'JSON' and '"AnswerSection": []' in r.text) or
             (format == 'XML') or
             ('No data was found' in r.text)), 'format is %s' % format

def test_idn(prefix): 
    for format in formats:
        r = requests.get(prefix + 'potamochère.fr/SOA%s' % fstring(format))
        assert  r.status_code == 200 and r.headers['content-type'] == formats[format] and \
            'hostmaster.gandi.net' in r.text, 'format is %s' % format

def test_content_negotiation(prefix):
    for format in formats:
        if format == '':
            continue
        r = requests.get(prefix + 'www.ietf.org/ADDR', headers={'Accept': sstring(format)})
        assert  r.status_code == 200 and r.headers['content-type'].startswith(formats[format]) and \
            '104.20.' in r.text, 'format is %s' % format

